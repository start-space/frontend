const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoiZXhhbXBsZTEiLCJyb2xlIjoidGVuYW50IiwiaWF0IjoxNjM3MDA1OTk0LCJleHAiOjE2NDIxODk5OTR9.jzLsOWSC6MZw289K-ot0tkODJwDzfhIR7dDDFMClOFk";
const getDataOutofStocks = () => {
    const headers = { 'Authorization': 'Bearer ' + token };
    const url = "http://68.183.181.55:4000/v1/items/tenant/categories?fields=items&isReady=false";
    return fetch(url, { headers })
        .then((response: any) => response.json())
        .then((resJson) => resJson.data)
        .catch((err) => {
            throw { message: err }
        });
}

export { getDataOutofStocks }