import { ComponentRef } from '@ionic/core';
import { toastController, loadingController, modalController } from '@ionic/vue';

const showToast = async (msg: string, color?: string) => {
    const toast = await toastController.create({
        animated: true,
        color: color,
        duration: 2000,
        message: msg,
        position: "bottom"
    });
    return toast.present();
}

const showLoading = async (message: string) => {
    const loading = await loadingController.create({
        animated: true,
        showBackdrop: true,
        backdropDismiss: false,
        message: message,
        spinner: 'bubbles'
    });
    return loading.present();
}

const dismissLoading = () => {
    return loadingController.dismiss();
}

const showModal = async (component: ComponentRef, props?: any, customCss?: string | string[]) => {
    const modal = await modalController.create({
        component: component,
        animated: true,
        backdropDismiss: false,
        componentProps: props,
        cssClass: customCss,
        swipeToClose: false,
        showBackdrop: true,
        keyboardClose: true
    });
    return modal;
    // return modal.present();
}

const dismissModal = (data?: any, role?: string) => {
    modalController.dismiss(data, role);
}

export { showToast, showLoading, dismissLoading, showModal, dismissModal }