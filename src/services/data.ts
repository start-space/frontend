import { showLoading, showToast, dismissLoading } from '@/services/common';

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoiZXhhbXBsZTEiLCJyb2xlIjoidGVuYW50IiwiaWF0IjoxNjM4MzAxNTkwLCJleHAiOjE2NDM0ODU1OTB9.VI8bid__iyta789VVEeK7C2oVJlX-r-hVi_n3rUOfI8";
const apiHost = "http://68.183.181.55:4000/v1";
const getData = () => {
  const headers = { 'Authorization': 'Bearer ' + token };
  return fetch(apiHost + "/items/tenant/categories?fields=items", { headers })
    .then((response: any) => response.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      throw { message: err }
    });
}
const getOptionGroups = async () => {
  const headers = { 'Authorization': 'Bearer ' + token };
  return fetch(apiHost + "/items/tenant/groups/list", { headers })
    .then((res) => res.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const getLinkedByOptionGroupsId = async (id: any) => {
  const headers = { 'Authorization': 'Bearer ' + token };
  return fetch(apiHost + '/items/tenant/groups/linked/' + id, { headers })
    .then((res) => res.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const itemLinked = async (data: any) => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  const { groupId, itemId } = data;
  const payload = { "menuItemId": itemId };
  return fetch(apiHost + '/items/tenant/groups/linked/' + groupId,
    {
      headers,
      method: "PUT",
      body: JSON.stringify(payload)
    })
    .then((res) => res.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const createOptionGroups = async (data: any) => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  return fetch(apiHost + "/items/tenant/groups/",
    {
      headers,
      body: JSON.stringify(data),
      method: "POST"
    })
    .then((res) => res.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const updateOptionGroups = async (id: any, data: any) => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  return fetch(apiHost + "/items/tenant/groups/" + id,
    {
      headers,
      body: JSON.stringify(data),
      method: "PUT"
    })
    .then((res) => res.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const pricePlatform = async () => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  return fetch(apiHost + "/items/tenant/prices/services",
    {
      headers,
      method: "GET"
    })
    .then((res) => res.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const getCategories = async () => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  return fetch(apiHost + '/items/tenant/categories', 
  {
    method: "GET",
    headers
  })
  .then((response) => response.json())
  .then((resJson) => resJson.data)
  .catch((err) => {
    console.error(err);
    throw err
  });
}

const createCategories = async (data: any) => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  return fetch(apiHost + "/items/tenant/categories",
    {
      headers,
      method: "POST",
      body: JSON.stringify(data)
    })
    .then((response) => response.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const updateCategories = async (id: any, data: any) => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  return fetch(apiHost + "/items/tenant/categories/" + id,
    {
      headers,
      method: "PUT",
      body: JSON.stringify(data)
    })
    .then((response) => response.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const deleteCategories = async (id: any) => {
  const headers = { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' };
  return fetch(apiHost + '/items/tenant/categories/' + id,
    {
      headers,
      method: "DELETE"
    })
    .then((response) => response.json())
    .then((resJson) => resJson.data)
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

const updateItems = async (id, data) => {
  const headers = { 'Authorization': 'Bearer ' + token};
  return fetch(apiHost + '/items/tenant/' + id, 
  {
    method: "PUT",
    headers,
    body: data
  })
  .then((response) => response.json())
  .then((resJson) => resJson.data)
  .catch((err) => {
    console.error(err);
    throw err;
  });
}

const getItemById = async (id) => {
  const headers = { 'Authorization': 'Bearer ' + token};
  return fetch(apiHost + '/items/tenant/' + id, 
  {
    method: "GET",
    headers
  })
  .then((response) => response.json())
  .then((resJson) => resJson.data)
  .catch((err) => {
    console.error(err);
    throw err;
  });
}

const updateItemsPromo = async (id, data) => {
  const headers = { 'Authorization': 'Bearer ' + token};
  return fetch(apiHost + '/items/tenant/promo/' + id, 
  {
    method: "PUT",
    headers,
    body: data
  })
  .then((response) => response.json())
  .then((resJson) => resJson.data)
  .catch((err) => {
    console.error(err);
    throw err;
  });
}

export {
  getData,
  getOptionGroups,
  getLinkedByOptionGroupsId,
  itemLinked,
  createOptionGroups,
  updateOptionGroups,
  pricePlatform,
  getCategories,
  createCategories,
  updateCategories,
  deleteCategories,
  updateItems,
  updateItemsPromo,
  getItemById
};