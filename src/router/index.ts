import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue';
import InitialPage from '../views/management/InitialPage.vue';
import HomeManagement from '../views/management/Home.vue';
import SetupMenus from '@/views/management/menus/setup/SetupMenus.vue';
import NewItems from '@/views/management/menus/setup/NewItems.vue';
import UpdateItems from '@/views/management/menus/setup/UpdateItems.vue';
import NewCategories from '@/views/management/menus/setup/NewCategories.vue';
import UpdateCategories from '@/views/management/menus/setup/UpdateCategories.vue';
import NewOptionGroups from '@/views/management/menus/setup/NewOptionGroups.vue';
import UpdateOptionGroups from '@/views/management/menus/setup/UpdateOptionGroups.vue';
import NewOption from '@/views/management/menus/setup/NewOption.vue';
import NewItemsPromo from '@/views/management/menus/setup/NewItemsPromo.vue';
import UpdateItemsPromo from '@/views/management/menus/setup/UpdateItemsPromo.vue';
import UpdateOption from '@/views/management/menus/setup/UpdateOption.vue';
import ReviewNewOptionGroups from '@/views/management/menus/setup/ReviewNewOptionGroups.vue';
import OptionLinkedItems from '@/views/management/menus/setup/OptionLinkedItems.vue';
import ItemsLinkedOption from '@/views/management/menus/setup/ItemsLinkedOption.vue';

const routes: Array<RouteRecordRaw> = [
  { path: '/',redirect: '/home'},
  {
    path: '/management/Home',
    name: 'HomeManagement',
    component: HomeManagement
  },
  { path: '/InitialPage', name: 'InitialPage', component: InitialPage},
  { path: '/SetupMenus', name: 'SetupMenus', component: SetupMenus},
  { path: '/NewItems', name: 'NewItems', component: NewItems},
  { path: '/UpdateItems', name: 'UpdateItems', component: UpdateItems},
  { path: '/NewItemsPromo', name: 'NewItemsPromo', component: NewItemsPromo},
  { path: '/UpdateItemsPromo', name: 'UpdateItemsPromo', component: UpdateItemsPromo},
  { path: '/NewCategories', name: 'NewCategories', component: NewCategories},
  { path: '/UpdateCategories', name: 'UpdateCategories', component: UpdateCategories},
  { path: '/NewOptionGroups', name: 'NewOptionGroups', component: NewOptionGroups},
  { path: '/UpdateOptionGroups', name: 'UpdateOptionGroups', component: UpdateOptionGroups, props: true},
  { path: '/NewOption', name: 'NewOption', component: NewOption},
  { path: '/UpdateOption', name: 'UpdateOption', component: UpdateOption},
  { path: '/ReviewNewOptionGroups', name: 'ReviewNewOptionGroups', component: ReviewNewOptionGroups},
  { path: '/OptionLinkedItems', name: 'OptionLinkedItems', component: OptionLinkedItems},
  { path: '/ItemsLinkedOption', name: 'ItemsLinkedOption', component: ItemsLinkedOption},
  { path: '/home', name: 'Home', component: Home},
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
