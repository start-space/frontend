{
    id: 2,
    category: "Minuman",
    tenantId: 1,
    createdAt: "2021-11-09T15:07:42.725Z",
    updatedAt: "2021-11-09T15:07:42.725Z",
    menuItems: [
        {
            image: "undefinedmenus/null",
            id: 3,
            itemName: "Minuman 1",
            description: "Description Minuman 1",
            stock: 10,
            isReady: true,
            ordering: 1,
            menuItemCategoryId: 2,
            createdAt "2021-11-09T15:07:42.789Z",
            updatedAt: "2021-11-09T15:07:42.789Z",
            menuItemPrices: [
                {
                    id: 11,
                    price: 10000,
                    pricePlatformId: 1,
                    menuItemId: 3,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 1,
                        platform: "Dine-in",
                        code: "dine_in",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 12,
                    price: 10000,
                    pricePlatformId: 2,
                    menuItemId: 3,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 2,
                        platform: "Gofood",
                        code: "gofood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 13,
                    price: 10000,
                    pricePlatformId: 3,
                    menuItemId: 3,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 3,
                        platform: "Grabfood",
                        code: "grabfood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 14,
                    price: 10000,
                    pricePlatformId: 4,
                    menuItemId: 3,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 4,
                        platform: "Shopeefood",
                        code: "shopeefood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 15,
                    price: 10000,
                    pricePlatformId: 5,
                    menuItemId: 3,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 5,
                        platform: "Traveloka Eat",
                        code: "traveloka_eat",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                }
            ],
            menuItemOptionGroups: []
        },
        {
            image: "undefinedmenus/null",
            id: 4,
            itemName: "Minuman 2",
            description: "Description Minuman 2",
            stock: 10,
            isReady: true,
            ordering: 2,
            menuItemCategoryId: 2,
            createdAt "2021-11-09T15:07:42.789Z",
            updatedAt: "2021-11-09T15:07:42.789Z",
            menuItemPrices: [
                {
                    id: 16,
                    price: 10000,
                    pricePlatformId: 1,
                    menuItemId: 4,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 1,
                        platform: "Dine-in",
                        code: "dine_in",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 17,
                    price: 10000,
                    pricePlatformId: 2,
                    menuItemId: 4,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 2,
                        platform: "Gofood",
                        code: "gofood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 18,
                    price: 10000,
                    pricePlatformId: 3,
                    menuItemId: 4,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 3,
                        platform: "Grabfood",
                        code: "grabfood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 19,
                    price: 10000,
                    pricePlatformId: 4,
                    menuItemId: 4,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 4,
                        platform: "Shopeefood",
                        code: "shopeefood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 20,
                    price: 10000,
                    pricePlatformId: 5,
                    menuItemId: 4,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 5,
                        platform: "Traveloka Eat",
                        code: "traveloka_eat",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                }
            ],
            menuItemOptionGroups: []
        }
    ]
},
{
    id: 1,
    category: "Makanan",
    tenantId: 1,
    createdAt: "2021-11-09T15:07:42.725Z",
    updatedAt: "2021-11-09T15:07:42.725Z",
    menuItems: [
        {
            image: "undefinedmenus/null",
            id: 1,
            itemName: "Makanan 1",
            description: "Description Makanan 1",
            stock: 10,
            isReady: true,
            ordering: 1,
            menuItemCategoryId: 1,
            createdAt "2021-11-09T15:07:42.789Z",
            updatedAt: "2021-11-09T15:07:42.789Z",
            menuItemPrices: [
                {
                    id: 1,
                    price: 10000,
                    pricePlatformId: 1,
                    menuItemId: 1,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 1,
                        platform: "Dine-in",
                        code: "dine_in",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 2,
                    price: 10000,
                    pricePlatformId: 2,
                    menuItemId: 1,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 2,
                        platform: "Gofood",
                        code: "gofood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 3,
                    price: 10000,
                    pricePlatformId: 3,
                    menuItemId: 1,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 3,
                        platform: "Grabfood",
                        code: "grabfood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 4,
                    price: 10000,
                    pricePlatformId: 4,
                    menuItemId: 1,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 4,
                        platform: "Shopeefood",
                        code: "shopeefood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 5,
                    price: 10000,
                    pricePlatformId: 5,
                    menuItemId: 1,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 5,
                        platform: "Traveloka Eat",
                        code: "traveloka_eat",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                }
            ],
            menuItemOptionGroups: []
        },
        {
            image: "undefinedmenus/null",
            id: 2,
            itemName: "Makanan 2",
            description: "Description Makanan 2",
            stock: 10,
            isReady: true,
            ordering: 2,
            menuItemCategoryId: 1,
            createdAt "2021-11-09T15:07:42.789Z",
            updatedAt: "2021-11-09T15:07:42.789Z",
            menuItemPrices: [
                {
                    id: 6,
                    price: 10000,
                    pricePlatformId: 1,
                    menuItemId: 2,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 1,
                        platform: "Dine-in",
                        code: "dine_in",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 7,
                    price: 10000,
                    pricePlatformId: 2,
                    menuItemId: 2,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 2,
                        platform: "Gofood",
                        code: "gofood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 8,
                    price: 10000,
                    pricePlatformId: 3,
                    menuItemId: 2,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 3,
                        platform: "Grabfood",
                        code: "grabfood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 9,
                    price: 10000,
                    pricePlatformId: 4,
                    menuItemId: 2,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 4,
                        platform: "Shopeefood",
                        code: "shopeefood",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                },
                {
                    id: 10,
                    price: 10000,
                    pricePlatformId: 5,
                    menuItemId: 2,
                    createdAt "2021-11-09T15:07:42.933Z",
                    updatedAt: "2021-11-09T15:07:42.933Z",
                    pricePlatform: {
                        id: 5,
                        platform: "Traveloka Eat",
                        code: "traveloka_eat",
                        logo: null,
                        createdAt "2021-11-09T15:07:42.871Z",
                        updatedAt: "2021-11-09T15:07:42.871Z"
                    }
                }
            ],
            menuItemOptionGroups: []
        }
    ]
}